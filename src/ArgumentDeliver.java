import entity.People;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by baoyunfeng on 2017/8/15.
 */
public class ArgumentDeliver {

    public static void main(String args[]){
        System.out.println("hello hasher");

        test1();
        test2();
        test3();
    }

    public static void test1(){

        Map<String, People> map = new HashMap<>();
        map.put("a",new People());
        map.put("b",new People());

        map.get("a").setName("new name");
        System.out.println("test1:"+map.get("a").getName());
    }

    public static void test2(){

        Map<String, People> map = new HashMap<>();
        map.put("a",new People());
        map.put("b",new People());

        People newPeople = map.get("a");
        newPeople.setName("new name");
        System.out.println("test2:"+map.get("a").getName());
    }


    /**
     ● 对于基本数据类型，由于其在栈中存放的是该类型变量的值，所以作为方法参数传递的时候将其值传递到方法内部。
     ● 对于引用数据类型，由于其在栈中只存放该类型变量的引用(变量地址)，所作为方法参数传递的时候将其引用传递给方法内部。
     */
    public static void test3(){
        Map<String, People> map = new HashMap<>();
        map.put("a",new People());
        map.put("b",new People());

        test3_up(map);
        System.out.println("test3:"+"obj:"+map.hashCode()+":"+map.get("a").getName());


        int data[] = new int[3];
        test3_data(data);
        System.out.println("test3:"+"data[0]:"+data[0]);
    }

    public static void test3_up(Map<String,People> map){
        People newPeople = map.get("a");
        newPeople.setName("new name");
        System.out.println("test3_up:"+"obj:"+map.hashCode());
    }

    public static void test3_data(int data[]){
        data[0] = 3;
    }
}
