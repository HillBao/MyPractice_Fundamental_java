/**
 * Created by baoyunfeng on 2018/1/24.
 */
public class BaseThreadOperation {


    public void vSleep(){
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void vWait(){
        //如果没有嵌套synchronized
        //Exception in thread "main" java.lang.IllegalMonitorStateException
        synchronized (this) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String args[]){
        BaseThreadOperation baseThreadOperation = new BaseThreadOperation();
        try {
            baseThreadOperation.wait();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
