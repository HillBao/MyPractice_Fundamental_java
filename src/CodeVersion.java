public class CodeVersion {

    public static void main(String args[]){

        System.out.println("Hello World!");
        System.out.println("compareTo(\"5.0.0\",\"4.9.9\"):"+compareTo("5.0.0","4.9.9"));
        System.out.println("compareTo(\"5.0.0\",\"5.0.0\"):"+compareTo("5.0.0","5.0.0"));
        System.out.println("compareTo(\"5.0.0\",\"5.0.1\"):"+compareTo("5.0.0","5.0.1"));
        System.out.println("compareTo(\"5.0.0\",\"5.0\"):"+compareTo("5.0.0","5.0"));
        System.out.println("compareTo(\"5.0.0\",\"5.0.0.0\"):"+compareTo("5.0.0","5.0.0.0"));
        System.out.println("compareTo(\"5.0.0\",\"x.x.x.x\"):"+compareTo("5.0.0","x.x.x.x"));
        System.out.println("compareTo(\"5.0.0\",\"x.\"):"+compareTo("5.0.0","x."));
    }

    /**
     *
     * @param oldVersion
     * @param newVersion
     * @return 0 : oldVersion    1 : newVersion
     */
    private static int compareTo(String oldVersion,String newVersion){
        String olds[] = oldVersion.split("\\.");
        String news[] = newVersion.split("\\.");
        try {
            int tempOld;
            int tempNew;
            for(int i = 0; i< Math.max(olds.length,news.length) ;i++){
                if(i<olds.length){
                    tempOld = Integer.valueOf(olds[i]);
                }else{
                    return 1;
                }
                if(i<news.length){
                    tempNew = Integer.valueOf(news[i]);
                }else {
                    return 0;
                }
                if(tempOld < tempNew){
                    return 1;
                }else if(tempOld > tempNew){
                    return 0;
                }
            }
            return 0;
        }catch (Exception e){
            return 0;
        }
    }
}
