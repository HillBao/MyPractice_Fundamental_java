import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


// List 基础操作
public class DataMap {
    public static void main(String args[]){
        System.out.println("Hello DataMap");

        DataMap d = new DataMap();
        d.getList("aaa").remove("b");
        d.getList("aaa").add(0,"b");
        d.printList(d.getList("aaa"));
    }

    Map<String,List<String>> map = new HashMap();

    public DataMap(){
        List<String> list = new ArrayList<>();
        list.add("a");
        list.add("b");
        list.add("c");
        list.add("d");
        list.add("e");
        map.put("aaa",list);
    }

    public List<String> getList(String key){
        return map.get(key);
    }

    public void printList(List<String> list){
        for(String t : list){

            System.out.println("List:"+t);
        }
    }




}
