/**
 * Created by baoyunfeng on 2018/1/24.
 */
public class InnerClass {

    private int outerInt = 8;

    /*-------------静态内部类------------*/

    private int getStaticInnerClassInt() {
        return new StaticInnerClass().getA();
    }

    private void setStaticInnerClassInt(int outer) {
        new StaticInnerClass().setA(outer);
    }

    static class StaticInnerClass{
        //Non-static cant from a static context
        //private int a = outerInt;

        private int a = 2;

        public int getA() {
            return a;
        }

        public void setA(int a) {
            this.a = a;
        }
    }

    /*--------------匿名内部类------------*/

    private void getAnonymousInterfaceInt(AnonymousInterface anonymousInterface) {

    }

    private void getAnonymousAbstractInt(AnonymousAbstract anonymousAbstract) {

    }

    private void getAnonymousInnerClassInt() {
        getAnonymousAbstractInt(new AnonymousAbstract() {
            @Override
            int getAnonymousAbstractInt() {
                return outerInt;
            }

            @Override
            void setAnonymousAbstractInt(int i) {
                InnerClass.this.setOuterInt(i);
            }
        });
        getAnonymousInterfaceInt(new AnonymousInterface() {
            @Override
            public int getAnonymousInterfaceInt() {
                return outerInt;
            }

            @Override
            public void setAnonymousInterfaceInt(int i) {
                InnerClass.this.setOuterInt(i);
            }

        });
    }

    private void say(int i){

    }

    abstract class AnonymousAbstract {
        abstract int getAnonymousAbstractInt();
        abstract void setAnonymousAbstractInt(int i);
    }

    interface AnonymousInterface {
        int getAnonymousInterfaceInt();
        void setAnonymousInterfaceInt(int i);
    }


    /*--------------方法内部类------------*/


    private int getVoidInnerClassInt(int newO){

        //作用域无效
        //int temp = new VoidInnerClass().getA();

        int nb =2;

        class VoidInnerClass{

            private int a = outerInt;

            public int getA() {
                return a;
            }

            public void setA(int a) {
                this.a = a;
            }

            public void setAFromOuterClassInt(){
                //No Error
                setA(newO);
                System.out.print(newO);
            }
        }

        return new VoidInnerClass().getA();
    }

    /*--------------成员内部类------------*/

    public int getPrivateInnerClassInt() {
        new PrivateInnerClass().setA(outerInt);
        return new PrivateInnerClass().getA();
    }

    public void setPrivateInnerClassInt(int outer) {
        new PrivateInnerClass().setA(outer);
    }

    private class PrivateInnerClass{
        private int a = outerInt;

        public int getA() {
            return a;
        }

        public void setA(int a) {
            this.a = a;
        }
    }


    public void setOuterInt(int outerInt) {
        this.outerInt = outerInt;
    }



    public static void main(String[] args){
        new InnerClass().amethod(1);
    }



    String str=new String ("Between");
    public void amethod(int iArgs)
    {
        int it315=10;
        class Bicycle
        {
            public void sayHello()
            {
                System.out.println(str);
                System.out.println(iArgs);
                System.out.println(it315);//此处编译出错：InOut.java:13: local variable it315 is accessed from within inner class; needs to be declared final
            }
        }
    }
}
