/**
 * Created by baoyunfeng on 2017/9/14.
 */
public class Interest {

    private static final String TAG = "Interest";

    public static void main(String args[]) {

        //房贷
        float capitalAll = 1 * 10000;
        float interestRateOfYear_Room = 0.0441f;

        System.out.println("1w:1年房贷按月计算等额本息每月需还：" +
                getPayOfMouthByMouthCalculate(capitalAll, 1, interestRateOfYear_Room)+"rmb");
        System.out.println("1w:1年房贷按月计算等额本息本息合计：" +
                getPayAllByMouthCalculate(capitalAll, 1, interestRateOfYear_Room)+"rmb");
        System.out.println("1w:1年房贷按月计算等额本息利息合计：" +
                getInterestAllByMouthCalculate(capitalAll, 1, interestRateOfYear_Room)+"rmb");

        System.out.println("1w:1年房贷按日计算等额本息每月需还：" +
                getPayOfMouthByDayCalculate(capitalAll, 1, interestRateOfYear_Room)+"rmb");
        System.out.println("1w:1年房贷按日计算等额本息本息合计：" +
                getPayAllByDayCalculate(capitalAll, 1, interestRateOfYear_Room)+"rmb");
        System.out.println("1w:1年房贷按月计算等额本息利息合计：" +
                getInterestAllByDayCalculate(capitalAll, 1, interestRateOfYear_Room)+"rmb");


        System.out.println();
        System.out.println();
        System.out.println();

        //高利贷
        float interestRateOfYear_High = 0.24f;
        System.out.println("1w:1年24%高利贷按月计算等额本息每月需还：" +
                getPayOfMouthByMouthCalculate(capitalAll, 1, interestRateOfYear_High)+"rmb");
        System.out.println("1w:1年24%高利贷按月计算等额本息本息合计：" +
                getPayAllByMouthCalculate(capitalAll, 1, interestRateOfYear_High)+"rmb");
        System.out.println("1w:1年24%高利贷按月计算等额本息利息合计：" +
                getInterestAllByMouthCalculate(capitalAll, 1, interestRateOfYear_High)+"rmb");

        System.out.println("1w:1年24%高利贷按日计算等额本息每月需还：" +
                getPayOfMouthByDayCalculate(capitalAll, 1, interestRateOfYear_High)+"rmb");
        System.out.println("1w:1年24%高利贷按日计算等额本息本息合计：" +
                getPayAllByDayCalculate(capitalAll, 1, interestRateOfYear_High)+"rmb");
        System.out.println("1w:1年24%高利贷按月计算等额本息利息合计：" +
                getInterestAllByDayCalculate(capitalAll, 1, interestRateOfYear_High)+"rmb");


        System.out.println();
        System.out.println();
        System.out.println();

        //借呗
        float interestRateOfDay_Jie = 0.0004f;
        float interestRateOfYear_Jie = interestRateOfDay_Jie * 365;
        System.out.println("1w:1年借呗按月计算等额本息每月需还：" +
                getPayOfMouthByMouthCalculate(capitalAll, 1, interestRateOfYear_Jie)+"rmb");
        System.out.println("1w:1年借呗按月计算等额本息本息合计：" +
                getPayAllByMouthCalculate(capitalAll, 1, interestRateOfYear_Jie)+"rmb");
        System.out.println("1w:1年借呗按月计算等额本息利息合计：" +
                getInterestAllByMouthCalculate(capitalAll, 1, interestRateOfYear_Jie)+"rmb");

        System.out.println("1w:1年借呗按日计算等额本息每月需还：" +
                getPayOfMouthByDayCalculate(capitalAll, 1, interestRateOfYear_Jie)+"rmb");
        System.out.println("1w:1年借呗按日计算等额本息本息合计：" +
                getPayAllByDayCalculate(capitalAll, 1, interestRateOfYear_Jie)+"rmb");
        System.out.println("1w:1年借呗按日计算等额本息利息合计：" +
                getInterestAllByDayCalculate(capitalAll, 1, interestRateOfYear_Jie)+"rmb");

    }


    public static double getPayOfMouthByMouthCalculate(float _capical, int _year, float _interestRateOfYear) {

        float capitalAll = _capical;
        float interestRateOfYear = _interestRateOfYear;
        float interestRateOfMouth = interestRateOfYear / 12f;
        int timeOfYear = _year;
        int timeOfMouth = timeOfYear * 12;
        int timeOfDay = timeOfMouth * 30;


        double tempInterestRate = (1 + interestRateOfMouth);
        tempInterestRate = Math.pow(tempInterestRate, timeOfMouth);
        /*for(int i = 0 ;i < timeOfYear ;i++){
            tempInterestRate *= (1+interestRateOfMouth);
        }*/

        double payOfMouth = (capitalAll * interestRateOfMouth * tempInterestRate) / (tempInterestRate - 1);

        return payOfMouth;
    }

    public static double getPayAllByMouthCalculate(float _capical, int _year, float _interestRateOfYear) {


        double payAll = getPayOfMouthByMouthCalculate(_capical, _year, _interestRateOfYear) * 12f * _year;

        return payAll;
    }


    public static double getInterestAllByMouthCalculate(float _capical, int _year, float _interestRateOfYear) {


        double interestAll = getPayAllByMouthCalculate(_capical, _year, _interestRateOfYear) - _capical;

        return interestAll;
    }



    public static double getPayOfDayByDayCalculate(float _capical, int _year, float _interestRateOfYear) {

        float capitalAll = _capical;
        float interestRateOfYear = _interestRateOfYear;
        float interestRateOfMouth = interestRateOfYear / 12f;
        float interestRateOfDay = interestRateOfYear / 30f;
        int timeOfYear = _year;
        int timeOfMouth = timeOfYear * 12;
        int timeOfDay = timeOfMouth * 30;


        double tempInterestRate = (1 + interestRateOfDay);
        tempInterestRate = Math.pow(tempInterestRate, timeOfDay);
        /*for(int i = 0 ;i < timeOfYear ;i++){
            tempInterestRate *= (1+interestRateOfMouth);
        }*/

        double payOfDay = (capitalAll * interestRateOfDay * tempInterestRate) / (tempInterestRate - 1);

        return payOfDay;
    }


    public static double getPayOfMouthByDayCalculate(float _capical, int _year, float _interestRateOfYear) {
        return getPayOfDayByDayCalculate(_capical, _year, _interestRateOfYear) * 30;
    }

    public static double getPayAllByDayCalculate(float _capical, int _year, float _interestRateOfYear) {
        return getPayOfMouthByDayCalculate(_capical, _year, _interestRateOfYear) * 12 * _year;
    }



    public static double getInterestAllByDayCalculate(float _capical, int _year, float _interestRateOfYear) {

        double interestAll = getPayAllByDayCalculate(_capical, _year, _interestRateOfYear) - _capical;

        return interestAll;
    }

}
