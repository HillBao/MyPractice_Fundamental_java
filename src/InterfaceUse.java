import java.util.Date;

/**
 * Created by baoyunfeng on 2018/1/24.
 */
public class InterfaceUse {


    public static void main(String[] args){
        TestInterface testInterface = new TestInterface() {
            @Override
            public void testC() {

            }


            @Override
            public void testA() throws Exception {

            }

            @Override
            //如果testB() throws Exception
            //报错overridden method does not throws Exception
            public void testB(){

            }

            @Override
            public void test() throws Exception{

            }
        };
    }

    interface TestInterface extends TestInterfaceA,TestInterfaceB,TestInterfaceC{
        //修饰符只能是public static final
        int s = new Date().getDate();

        //隐式修饰符只能是public abstract
        void test() throws Exception;
    }
    interface TestInterfaceA {

        final int a = 2;
        static int b = 2;
        final static int c = 2;

        void testA() throws Exception;
    }
    interface TestInterfaceB{
        void testB();
    }
    interface TestInterfaceC{

        void testC();
    }
}
