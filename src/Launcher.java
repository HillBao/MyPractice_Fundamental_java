import lock.BaseLock;

/**
 * Created by baoyunfeng on 2017/1/17.
 */
public class Launcher {

    public void startBaseLock(){
        BaseLock.SynchronizedRunnable synchronizedRunnable = new BaseLock.SynchronizedRunnable();
        for(int i = 0 ;i<5;i++){
            Thread thread = new Thread(synchronizedRunnable,String.valueOf(i));
            thread.start();
        }
    }
}
