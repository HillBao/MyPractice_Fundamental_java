/**
 * Created by baoyunfeng on 2018/1/21.
 *
 *
○ 重载发生在一个类中，同名的方法如果有不同参数列表则视为重载
 ■ 不能通过抛出的异常类型来重载fun方法。
 ■ 不能通过返回值重载fun方法。
 ■ 不能通过不同的访问权限进行重载
 */
public class Overload {


    static class A{
        public void fun(){

        }

        public void fun2(String str,int i){

        }

        public void fun2(int i, String str){

        }

        protected void fun3(){

        }

        protected void fun4(){

        }
    }

    static class B extends A{

        @Override
        public void fun() {
            super.fun();
        }


        public void fun(int i){

        }

        //这个方法不属于重载，是这个类一个新的方法
        public void fun3(){

        }

        //访问权限不能比父类中被重写的方法的访问权限更低
        /*private void fun4(){
            //Error
        }*/
    }

    abstract class Abstract{
        abstract void fun();
    }

    class Impl extends Abstract{
        @Override
        synchronized void fun() {

        }
    }


    /*class Inner {}

    public static void foo() { new Inner(); }

    public void bar() { new Inner(); }

    public static void main(String[] args) {
        new Inner();
    }*/
}
