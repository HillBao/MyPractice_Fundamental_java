package lock;

/**
 * Created by baoyunfeng on 2017/1/17.
 */
public class BaseLock {

    public static class SynchronizedRunnable implements Runnable {
        @Override
        public void run() {
            synchronized (this) {
                for (int i = 0; i < 5; i++) {
                    System.out.println(Thread.currentThread().getName() + "  synchronized loop:" + i);
                }
            }
        }

    }
}