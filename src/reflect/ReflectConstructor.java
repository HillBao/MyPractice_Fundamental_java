package reflect;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/**
 * Created by baoyunfeng on 2018/1/8.
 */
public class ReflectConstructor {

    public static void main(String[] args) throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {

        Class clazz = Class.forName("reflect.UserModel");

        System.out.println("第一种方法，实例化默认构造方法，User必须无参构造函数,否则将抛异常");

        UserModel user1 = (UserModel) clazz.newInstance();
        System.out.println(user1.toString());
        System.out.println("--------------------------------------------");


        System.out.println("第二种方法，获取带String参数的public构造函数");

        Constructor constructor2 = clazz.getConstructor(String.class);
        UserModel user2 = (UserModel) constructor2.newInstance("hello");
        System.out.println(user2.toString());

        System.out.println("--------------------------------------------");


        System.out.println("第三种方法，取得指定带int和String参数构造函数,该方法是私有构造private");
        Constructor constructor3 = clazz.getDeclaredConstructor(int.class,String.class);
        //由于是private必须设置可访问
        constructor3.setAccessible(true);
        UserModel user3 = (UserModel) constructor3.newInstance(24,"sibio");
        System.out.println(user3.toString());


        System.out.println("-----getDeclaringClass-----");
        Class uclazz=constructor3.getDeclaringClass();
        //Constructor对象表示的构造方法的类
        System.out.println("构造方法的类:"+uclazz.getName());
        System.out.println("-----getParameterTypes-----");
        //获取构造函数参数类型
        Class<?> clazzs3[] = constructor3.getParameterTypes();
        for (Class claz:clazzs3) {
            System.out.println("参数名称:"+claz.getName());
        }

        System.out.println("--------------------------------------------");


        System.out.println("查看所有public构造函数");
        Constructor constructors[] = clazz.getConstructors();
        for(int i = 0 ;i< constructors.length;i++){
            Class<?> clazzs[] = constructors[i].getParameterTypes();
            System.out.println("构造函数["+i+"]:"+constructors[i].toString() );
            System.out.print("参数类型["+i+"]:(");
            for (int j = 0; j < clazzs.length; j++) {
                if (j == clazzs.length - 1)
                    System.out.print(clazzs[j].getName());
                else
                    System.out.print(clazzs[j].getName() + ",");
            }
            System.out.println(")");
        }


        System.out.println("查看所有构造函数");
        Constructor constructors1[] = clazz.getDeclaredConstructors();
        for(int i = 0 ;i< constructors1.length;i++){
            Class<?> clazzs[] = constructors1[i].getParameterTypes();
            System.out.println("构造函数["+i+"]:"+constructors1[i].toString() );
            System.out.print("参数类型["+i+"]:(");
            for (int j = 0; j < clazzs.length; j++) {
                if (j == clazzs.length - 1)
                    System.out.print(clazzs[j].getName());
                else
                    System.out.print(clazzs[j].getName() + ",");
            }
            System.out.println(")");
        }


    }
}
