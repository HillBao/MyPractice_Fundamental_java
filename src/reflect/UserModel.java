package reflect;

/**
 * Created by baoyunfeng on 2018/1/8.
 */
public class UserModel {


    private int age;
    private String name;
    public String publicName;

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UserModel() {
    }

    public UserModel(String name) {
        this.name = name;
    }

    private UserModel(int age, String name) {
        this.age = age;
        this.name = name;
    }

    @Override
    public String toString() {
        return "UserModel{" +
                "age=" + age +
                ", name='" + name + '\'' +
                '}';
    }
}
