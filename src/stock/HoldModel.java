package stock;

import java.util.*;

/**
 * Created by baoyunfeng on 2018/2/2.
 */
public class HoldModel {
    private String name;
    private List<StockModel> buyStockList = new ArrayList<>();
    private List<StockModel> sellStockList = new ArrayList<>();
    private float allCNTNumber;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getAllCNTNumber() {
        return allCNTNumber;
    }

    public void setAllCNTNumber(float allCNTNumber) {
        this.allCNTNumber = allCNTNumber;
    }

    public void addStockModel(float price, float number){
        if(number>0){
            buyStockList.add(new StockModel(price,number));
        }else{
            sellStockList.add(new StockModel(price,number));
        }
    }

    public List<StockModel> getBuyStockList() {
        Collections.sort(buyStockList);
        return buyStockList;
    }

    public void setBuyStockList(List<StockModel> buyStockList) {
        this.buyStockList = buyStockList;
    }

    public List<StockModel> getSellStockList() {
        Collections.sort(sellStockList);
        return sellStockList;
    }

    public void setSellStockList(List<StockModel> sellStockList) {
        this.sellStockList = sellStockList;
    }


}
