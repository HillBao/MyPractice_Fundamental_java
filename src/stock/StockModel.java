package stock;

/**
 * Created by baoyunfeng on 2018/2/2.
 */
public class StockModel implements Comparable<StockModel>{
    private float price;
    private float number;

    public StockModel(float price, float number) {
        this.price = price;
        this.number = number;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public float getNumber() {
        return number;
    }

    public void setNumber(float number) {
        this.number = number;
    }


    @Override
    public int compareTo(StockModel o) {
        return this.price-o.getPrice()>0?1:-1;
    }
}
