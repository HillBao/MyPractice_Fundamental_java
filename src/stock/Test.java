package stock;

/**
 * Created by baoyunfeng on 2018/2/2.
 */
public class Test {


    public static void main(String args[]) {
//        PriceControl priceControl = new PriceControl();
//        priceControl.setData(generateHoldModel());
//        priceControl.run(0.075f);



        PriceControl priceControl2 = new PriceControl();
        priceControl2.setData(generateHoldModel2());
        priceControl2.run(0.075f);


//        PriceControl priceControl3 = new PriceControl();
//        priceControl3.setData(generateHoldModel3());
//        priceControl3.run(0.075f);
    }


    public static HoldModel generateHoldModel() {
        HoldModel holdModel = new HoldModel();
        holdModel.setName("swt");
        holdModel.setAllCNTNumber(1000);
        holdModel.addStockModel(0.099f, 5000);
        holdModel.addStockModel(0.091f, -3000);
        holdModel.addStockModel(0.09f, 3000);
        holdModel.addStockModel(0.0899f, -3000);
        holdModel.addStockModel(0.09175f, -2000);
        holdModel.addStockModel(0.095f, 2000);
        holdModel.addStockModel(0.0976f, -2000);
        holdModel.addStockModel(0.0898f, 5000);
        holdModel.addStockModel(0.1198f, -2000);
        holdModel.addStockModel(0.1125f, -3000);
        holdModel.addStockModel(0.1496f, 2000);
        holdModel.addStockModel(0.1498f, 1800);
        return holdModel;
    }



    public static HoldModel generateHoldModel2() {
        HoldModel holdModel = new HoldModel();
        holdModel.setName("swt");
        holdModel.setAllCNTNumber(5500);
        holdModel.addStockModel(0.099f, 5000);
        holdModel.addStockModel(0.091f, -3000);
        holdModel.addStockModel(0.09f, 3000);
        holdModel.addStockModel(0.0899f, -3000);
        holdModel.addStockModel(0.09175f, -2000);
        holdModel.addStockModel(0.095f, 2000);
        holdModel.addStockModel(0.0976f, -2000);
        holdModel.addStockModel(0.0898f, 5000);
        holdModel.addStockModel(0.1198f, -2000);
        holdModel.addStockModel(0.1125f, -3000);
        holdModel.addStockModel(0.1496f, 2000);
        holdModel.addStockModel(0.1498f, 1800);
        holdModel.addStockModel(0.189f, 10000);
        holdModel.addStockModel(0.188f, 16200);
        holdModel.addStockModel(0.1829f, 300);
        holdModel.addStockModel(0.18f, -10000);
        holdModel.addStockModel(0.167f, 10000);
        holdModel.addStockModel(0.1605f, 900);
        holdModel.addStockModel(0.175f, -10000);
        holdModel.addStockModel(0.175f, 10000);
        holdModel.addStockModel(0.179f, -10000);
        holdModel.addStockModel(0.19f, 9000);
        holdModel.addStockModel(0.1886f, -10000);
        holdModel.addStockModel(0.195f, 10000);
//        holdModel.addStockModel(0.265f, -10000);
//        holdModel.addStockModel(0.25f, 5000);
//        holdModel.addStockModel(0.28f, 5000);
//        holdModel.addStockModel(0.277f, -5000);
//        holdModel.addStockModel(0.278f, 10000);
//        holdModel.addStockModel(0.288f, -15000);
//        holdModel.addStockModel(0.287f, 15000);
//        holdModel.addStockModel(0.287f, -6000);
//        holdModel.addStockModel(0.288f, 6000);
//        holdModel.addStockModel(0.249f, -10000);
//        holdModel.addStockModel(0.22f, -20000);
//        holdModel.addStockModel(0.27f, 20000);
//        holdModel.addStockModel(0.28f, 5000);
//        holdModel.addStockModel(0.263f, -5000);
//        holdModel.addStockModel(0.27f, -5000);
//        holdModel.addStockModel(0.271f, -5200);
//        holdModel.addStockModel(0.28f, 10000);
//        holdModel.addStockModel(0.27f, 2800);
//        holdModel.addStockModel(0.271f, 1900);
//        holdModel.addStockModel(0.243f, 80);
//        holdModel.addStockModel(0.26f, -10000);
//        holdModel.addStockModel(0.25f, 10000);
//        holdModel.addStockModel(0.23f, -10000);
//        holdModel.addStockModel(0.21f, -6000);
//        holdModel.addStockModel(0.2f, 2000);
//        holdModel.addStockModel(0.205f, 4000);
//        holdModel.addStockModel(0.208f, 8000);
//        holdModel.addStockModel(0.21f, 4000);
//        holdModel.addStockModel(0.22f, 4000);
//        holdModel.addStockModel(0.23f, 7000);
//        holdModel.addStockModel(0.22f, -7000);
//        holdModel.addStockModel(0.217f, -5000);
//        holdModel.addStockModel(0.245f, -5000);
//        holdModel.addStockModel(0.2f, 10000);
//        holdModel.addStockModel(0.2f, 2400);
//        holdModel.addStockModel(0.18f, 8000);
//        holdModel.addStockModel(0.16f, -10000);
//        holdModel.addStockModel(0.2f, 8000);
//        holdModel.addStockModel(0.182f, -4000);
//        holdModel.addStockModel(0.16f, 4700);
//        holdModel.addStockModel(0.169f, -5000);
//        holdModel.addStockModel(0.17f, 4000);
//        holdModel.addStockModel(0.165f, -9000);
//        holdModel.addStockModel(0.148f, 10000);
//        holdModel.addStockModel(0.155f, -1000);
//        holdModel.addStockModel(0.161f, -8000);
//        holdModel.addStockModel(0.166f, -5000);
//        holdModel.addStockModel(0.165f, -5000);
//        holdModel.addStockModel(0.169f, -2000);
//        holdModel.addStockModel(0.168f, 10000);
//        holdModel.addStockModel(0.173f, 10000);
//        holdModel.addStockModel(0.164f, -8000);
//        holdModel.addStockModel(0.163f, 8000);
//        holdModel.addStockModel(0.166f, -2000);
//        holdModel.addStockModel(0.163f, 10000);
//        holdModel.addStockModel(0.165f, 9200);
//        holdModel.addStockModel(0.163f, 9300);
//        holdModel.addStockModel(0.138f, -5000);
//        holdModel.addStockModel(0.13f, 5000);
//        holdModel.addStockModel(0.12f, -5000);
//        holdModel.addStockModel(0.126f, 4700);
//        holdModel.addStockModel(0.117f, -7000);
//        holdModel.addStockModel(0.123f, 2000);
//        holdModel.addStockModel(0.122f, 4000);
        return holdModel;
    }



    public static HoldModel generateHoldModel3() {
        HoldModel holdModel = new HoldModel();
        holdModel.setName("swt");
        holdModel.setAllCNTNumber(1000);
        holdModel.addStockModel(0.099f, 5000);
        holdModel.addStockModel(0.091f, -3000);
        holdModel.addStockModel(0.09f, 3000);
        holdModel.addStockModel(0.0899f, -3000);
        holdModel.addStockModel(0.09175f, -2000);
        holdModel.addStockModel(0.095f, 2000);
        holdModel.addStockModel(0.0976f, -2000);
        holdModel.addStockModel(0.0898f, 5000);
        holdModel.addStockModel(0.1198f, -2000);
        holdModel.addStockModel(0.1125f, -3000);
        holdModel.addStockModel(0.1496f, 2000);
        holdModel.addStockModel(0.1498f, 1800);
        return holdModel;
    }



}
